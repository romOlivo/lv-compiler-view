import { TestBed } from '@angular/core/testing';

import { LvCompilerService } from './lv-compiler.service';

describe('LvCompilerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LvCompilerService = TestBed.get(LvCompilerService);
    expect(service).toBeTruthy();
  });
});
