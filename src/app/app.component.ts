import {Component, Input} from '@angular/core';
import { LvCompilerService } from './lv-compiler.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'lv-compiler-view';
  constructor(private compiler: LvCompilerService) {
    compiler.code = 'a';
  }

  @Input('code')
  set code(code: string) {
    this.compiler.code = code;
  }

  get code(): string { return this.compiler.code; }
}
