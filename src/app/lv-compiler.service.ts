import {Injectable, Input} from '@angular/core';
import * as node from 'node';

@Injectable({
  providedIn: 'root'
})
export class LvCompilerService {
  private myCode: string;
  public log: string[];
  private compiler = require('./../assets/lv-languaje/compiler.js');

  constructor() { }

  @Input('code')
  set code(code: string) {
    this.myCode = code;
    this.log = this.compiler.compile(code).split(/\r?\n/);
  }

  get code(): string { return this.myCode; }

}
